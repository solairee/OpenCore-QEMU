# OpenCore EFI for QEMU/KVM (with single GPU Passthrough for Vega 56)

[![Based License](https://custom-icon-badges.herokuapp.com/badge/BASED_LICENSE-696969?logo=gigachad&style=for-the-badge)](https://github.com/thatonecalculator/BASED-LICENSE)

This is my EFI folder for the VM provided by the OSX-KVM project by Kholia.

It currently works fine, though there is no sound support except for my external audio interface.

Currently, the only tested GPU is the reference Radeon Vega 56, and it works properly. Please open an issue if you have problems with another Radeon card. I do not know much about fixing Nvidia cards, nor do I care to add support for them due to my dislike for Nvidia as a company. (Petty? Maybe. Do I care? no.)

The XML for the VM can be imported via this command: 
* `sudo virsh define macOS-QEMU.xml`

The default XML configuration is set up to my personal machine's specs (8 threads and 12 gigs to the VM), please do not forget to adjust to your computer's specs.

iServices *should* be broken, though I intend on fixing them.

Any help on improving this project would be much appreciated.